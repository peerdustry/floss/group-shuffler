# GroupShuffler

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/group_shuffler`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Install the gem and add to the application's Gemfile by executing:

    $ bundle add group_shuffler

If bundler is not being used to manage dependencies, install the gem by executing:

    $ gem install group_shuffler

## Usage

* Edit the [people.yml][people.yml] file with your preferences
* Enter inside the ruby console: `irb`
* Uses as follows:
```ruby
require_relative 'lib/group_shuffler/shuffler'
GroupShuffler::Shuffler.create
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/group_shuffler.
