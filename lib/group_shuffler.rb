# frozen_string_literal: true

require_relative "group_shuffler/version"

module GroupShuffler
  class Error < StandardError; end
  # Your code goes here...
end
