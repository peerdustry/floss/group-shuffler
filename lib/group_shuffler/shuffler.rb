require_relative 'config'

module GroupShuffler
  module Shuffler

    def self.create
      config = GroupShuffler::Config.configure_with_yml
      groups = config['groups']
      participants = config['participants']
      keys = config['keys']

      # Initialize the groups
      final_groups = {}
      groups.times.each do |count|
        final_groups[count] = []
      end

      allocated = 0
      # Shuffle key people
      keys.shuffle.each_with_index do |key|
        final_groups[allocated % groups] << key
        allocated += 1
      end

      # Shuffle other participants
      (participants - keys).shuffle.each_with_index do |participant|
        final_groups[allocated % groups] << participant
        allocated += 1
      end

      # Pretty printing the result
      final_groups.each do |index, names|
        puts "Social #{index + 1}"
        names.shuffle.each do |name|
          puts "--> #{name}"
        end
        puts "\n"
      end
    end
  end
end
